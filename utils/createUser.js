const readline = require('readline');
const { validate } = require('email-validator');
const { createUser } = require('./auth');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let email, pass;

function question (quest) {
  return new Promise(resolve => {
    rl.question(quest, resolve);
  });
}

async function getEmail () {
  email = await question('Input email: ');
  if (!validate(email)) {
    console.log('Email isn\'t valid');
    await getEmail();
  }
}

async function getPass () {
  pass = await question('Input password: ');
  if (pass.length < 6) {
    console.log('Password length has be more then 6 symbols');
    await getPass();
  }
}

(async () => {
  try {
    await getEmail();
    await getPass();
    await createUser(email, pass);

    console.log('User is added succefully!');
    rl.close();
  } catch (err) {
    console.error(err);
  }
})();
