const db = require('../models/db');
const crypto = require('crypto');

function hash (text) {
  return crypto.createHash('sha1').update(text).digest('base64');
}

exports.createUser = (email, password) => {
  const hashedPass = hash(password);
  return db.set('user', {
    email,
    password: hashedPass
  }).write();
};

exports.checkUser = (email, password) => {
  const user = db.get('user').value();
  return (user.email === email) && (user.password === hash(password));
};

exports.checkAuth = function (req, res, next) {
  if (req.session.authorised) {
    // если авторизованы и пытаемся зайти на страницу логина, то редиректим на страницу админки
    if (req.route.path === '/login') {
      return res.redirect(303, '/admin');
    }
    // иначе пускаем дальше
    next();
  } else {
    // если не авторизованы и запрашиваем страницу логина, то пускаем дальше
    if (req.route.path === '/login') {
      return next();
    }
    // иначе просим авторизацию
    res.redirect(303, '/login');
  }
};
