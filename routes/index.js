const express = require('express');
const router = express.Router();

const { checkAuth } = require('../utils/auth');

const { getIndex, postIndex } = require('../controllers/index');
const { getLogin, postLogin, getAdmin, postSkill, uploadProduct } = require('../controllers/admin');

router.get('/', getIndex);
router.post('/', postIndex);

router.get('/login', checkAuth, getLogin);
router.post('/login', checkAuth, postLogin);

router.get('/admin', checkAuth, getAdmin);
router.post('/admin/skills', checkAuth, postSkill);
router.post('/admin/upload', checkAuth, uploadProduct);

module.exports = router;
