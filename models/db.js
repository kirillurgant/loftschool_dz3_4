const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const { resolve } = require('path');

const adapter = new FileSync(resolve(process.cwd(), 'models/db.json'));
const db = low(adapter);

module.exports = db;
