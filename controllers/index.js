const nodemailer = require('nodemailer');
const { smtp } = require('../configs/nodemailer');
const db = require('../models/db');

exports.getIndex = (req, res) => {
  const skills = db.get('skills').value();
  const products = db.get('products').value();

  res.render('pages/index', {
    skills,
    products
  });
};

exports.postIndex = (req, res, next) => {
  const { name, email, message } = req.body;
  if (!(name && email && message)) {
    const err = new Error('Заполните все поля!');
    err.status = 400;
    next(err);
    return;
  }

  const transporter = nodemailer.createTransport(smtp);
  const mailOptions = {
    from: `"${name}" <${email}>`,
    to: smtp.auth.user,
    subject: 'Фидбек с сайта',
    text:
      message.trim().slice(0, 500) +
      `\n Отправлено с: <${email}>`
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      next(err);
      return;
    }

    res.json({
      message: 'Успешно отправлено!'
    });
  });
};
