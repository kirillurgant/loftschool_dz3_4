const { checkUser } = require('../utils/auth');
const db = require('../models/db');
const formidable = require('formidable');
const { resolve } = require('path');
const fs = require('fs');

exports.getLogin = (req, res) => {
  res.render('pages/login');
};

exports.postLogin = async (req, res, next) => {
  const { email, password } = req.body;
  if (!(email && password)) {
    const err = new Error('Заполните все поля!');
    err.status = 400;
    next(err);
    return;
  }

  try {
    if (await !checkUser(email, password)) {
      next(new Error('Неверный email или пароль'));
      return;
    }

    req.session.authorised = true;
    res.redirect(303, '/admin');
  } catch (err) {
    next(err);
  }
};

exports.getAdmin = (req, res) => {
  res.render('pages/admin');
};

exports.postSkill = (req, res, next) => {
  const { age, concerts, cities, years } = req.body;
  const skills = db.get('skills').value();

  if (!(age && concerts && cities && years)) {
    const err = new Error('Заполните все поля!');
    err.status = 400;
    next(err);
    return;
  }

  try {
    [age, concerts, cities, years].forEach((item, index) => {
      skills[index].number = item;
    });

    db.set('skills', skills).write();

    res.json({
      message: 'Счетчик успешно сохранен'
    });
  } catch (err) {
    next(err);
  }
};

exports.uploadProduct = (req, res, next) => {
  const form = new formidable.IncomingForm();
  const uploadDir = resolve(process.cwd(), 'public/assets/img/products');

  if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir);
  }

  form.uploadDir = uploadDir;

  form.parse(req, (err, fields, files) => {
    const { name, price } = fields;
    const fileName = resolve(uploadDir, files.photo.name);

    if (err) {
      return next(err);
    }

    if (!(name && price)) {
      const err = new Error('Заполните все поля!');
      err.status = 400;
      return next(err);
    }

    if (!files.photo.size) {
      const err = new Error('Выберите файл!');
      err.status = 400;
      return next(err);
    }

    fs.rename(files.photo.path, fileName, err => {
      if (err) {
        return next(err);
      }

      db.get('products').push({
        src: `./assets/img/products/${files.photo.name}`,
        name,
        price
      }).write();

      res.json({
        message: 'Товар успешно добавлен!'
      });
    });
  });
};
