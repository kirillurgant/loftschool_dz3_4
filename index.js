const express = require('express');
const app = express();
const { resolve } = require('path');
const router = require('./routes/index');
const session = require('express-session');
const FileStore = require('session-file-store')(session);

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.set('views', './source/template');
app.set('view engine', 'pug');

app.use(express.static(resolve(__dirname, 'public'), { index: false }));

app.use(
  session({
    store: new FileStore(),
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
  })
);

app.use('/', router);

app.use((err, req, res, next) => {
  const { status, stack } = err;
  res.status(status || 500);
  res.json({
    status,
    err: stack
  });
});

app.listen(3000, () => {
  console.log('App listening on port 3000!');
});
